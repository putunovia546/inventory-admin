<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GoodCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('good_categories')->insert(
            [
                [
                    'name' => 'Retail',
                ],
                [
                    'name' => 'Wholesale'
                ]
            ]);
    }
}
