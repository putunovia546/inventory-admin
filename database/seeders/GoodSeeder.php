<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('goods')->insert(
            [
                [
                    'name' => "Headset",
                    'category_id' => "1",
                    'price' => "40000",

                ],
                [
                    'name' => "Pencil",
                    'category_id' => "2",
                    'price' => "5000",
                ],
                [
                    'name' => "A4 Paper",
                    'category_id' => "2",
                    'price' => "35000",
                ],
                [
                    'name' => "USB Cable",
                    'category_id' => "1",
                    'price' => "15000",
                ]
            ]);
    
    }
}
