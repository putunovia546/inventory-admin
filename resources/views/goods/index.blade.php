@extends('layouts.app', ['status' => 'complete'])

@section('content')
<section>
    <table  class="table table-bordered data-table">
        <thead>
            <tr>
                <th>#</th>
                <th>Goods</th>
                <th>Category</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</section>

@endsection