<?php

namespace App\Http\Controllers;

use App\Models\Good;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class GoodController extends Controller
{
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = Good::get();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
     
                           $btn = '<a href="javascript:void(0)" class="edit font-weight-bold btn-sm">Edit</a> | <a href="javascript:void(0)" class="delete font-weight-bold">Delete</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        
        return view('goods.index');
    }
   
}
