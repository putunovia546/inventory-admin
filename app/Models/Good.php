<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'category_id',
        'price'
    ];

    public function good_categories()
    {
        return $this->belongsTo(GoodCategories::class);
    }
}
