<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoodCategories extends Model
{
    use HasFactory;

    protected $fillable = 'name';

    public function good(){
        return $this->hasMany(Good::class);
    }
    
}
